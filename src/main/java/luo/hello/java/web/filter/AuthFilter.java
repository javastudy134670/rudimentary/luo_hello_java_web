package luo.hello.java.web.filter;


import luo.hello.java.web.db.model.UserModel;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter("/admin/*")
public class AuthFilter implements Filter {
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        UserModel user = (UserModel) request.getSession().getAttribute("USER_INFO");
        if (user != null && user.role().equals("super")) {
            filterChain.doFilter(request, response);
        } else {
            response.sendRedirect("../index.jsp");
        }
    }
}
