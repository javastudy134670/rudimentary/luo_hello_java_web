package luo.hello.java.web;

import java.io.PrintWriter;

public interface WriteResponse {
    void write(PrintWriter writer);
}