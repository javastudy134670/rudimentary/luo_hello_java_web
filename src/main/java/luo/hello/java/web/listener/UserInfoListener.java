package luo.hello.java.web.listener;

import luo.hello.java.web.db.model.UserModel;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionBindingEvent;
import java.util.ArrayList;
import java.util.Collection;

@WebListener
public class UserInfoListener implements HttpSessionAttributeListener {
    @Override
    public void attributeRemoved(HttpSessionBindingEvent se) {
        String name = se.getName();
        if (name.equals("USER_INFO")) {
            UserModel user = (UserModel) se.getValue();
            Object attribute = se.getSession().getServletContext().getAttribute("aliveUsers");
            Collection<UserModel> aliveUsers = new ArrayList<>();
            if (attribute instanceof Collection<?> collection) {
                collection.forEach(each -> {
                    UserModel eachUser = (UserModel) each;
                    if (eachUser.id() != user.id()) {
                        aliveUsers.add(eachUser);
                    }
                });
            }
            se.getSession().getServletContext().setAttribute("aliveUsers", aliveUsers);
        }
    }

    @Override
    public void attributeAdded(HttpSessionBindingEvent se) {
        String name = se.getName();
        if (name.equals("USER_INFO")) {
            UserModel user = (UserModel) se.getValue();
            Object attribute = se.getSession().getServletContext().getAttribute("aliveUsers");
            if (attribute == null) {
                attribute = new ArrayList<UserModel>();
            }
            Collection<UserModel> aliveUsers = new ArrayList<>();
            if (attribute instanceof Collection<?> collection) {
                collection.forEach(each -> aliveUsers.add((UserModel) each));
            }
            aliveUsers.add(user);
            se.getSession().getServletContext().setAttribute("aliveUsers", aliveUsers);
        }
    }
}
