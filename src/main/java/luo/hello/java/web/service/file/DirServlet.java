package luo.hello.java.web.service.file;

import luo.hello.java.web.WriteResponse;
import luo.hello.java.web.db.Conn;
import luo.hello.java.web.db.dao.DirDao;
import luo.hello.java.web.db.dao.FileDao;
import luo.hello.java.web.db.model.DirModel;
import luo.hello.java.web.db.model.FileModel;
import luo.hello.java.web.db.model.UserModel;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

@WebServlet("/user/*")
public class DirServlet extends HttpServlet {
    UserModel user;
    String uri;
    String dirPath;

    DirDao dirDao;
    FileDao fileDao;

    DirModel dirModel;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (verify(request)) {
            try {
                dirModel = dirDao.findByName(dirPath, user.id());
                if (dirModel == null) {
                    response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                    Conn.sqlError.write(response.getWriter());
                    return;
                }
                Collection<FileModel> files = new ArrayList<>();
                Collection<DirModel> dirs = new ArrayList<>();
                dirDao.findAll().forEach(each -> {
                    if (each.dirId() == dirModel.id()) {
                        dirs.add(each);
                    }
                });
                fileDao.findAll().forEach(each -> {
                    if (each.dirId() == dirModel.id()) {
                        files.add(each);
                    }
                });
                request.setAttribute("user", user);
                request.setAttribute("dirModel", dirModel);
                request.setAttribute("dirs", dirs);
                request.setAttribute("files", files);
                getServletContext().getRequestDispatcher("/filelist.jsp").forward(request, response);
            } catch (SQLException ignore) {
            }
        } else {
            response.setStatus(HttpServletResponse.SC_FORBIDDEN);
            forbidden.write(response.getWriter());
        }
    }

    private boolean verify(HttpServletRequest request) {
        user = (UserModel) request.getSession().getAttribute("USER_INFO");
        uri = request.getRequestURI();
        while (uri.endsWith("/")) {
            uri = uri.substring(0, uri.length() - 1);
        }
        int begin = uri.indexOf("user/") + 5;
        int end = uri.indexOf("/files");
        String uri_username = uri.substring(begin, end);
        dirPath = uri.substring(end + 1);
        return user != null && user.username().equals(uri_username);
    }

    @Override
    public void init() {
        ServletContext context = getServletContext();
        String url = context.getInitParameter("mysqlURL");
        String username = context.getInitParameter("username");
        String password = context.getInitParameter("password");
        try {
            dirDao = new DirDao(url, username, password);
            fileDao = new FileDao(url, username, password);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    WriteResponse forbidden = writer -> {
        writer.println("<html lang='zh'>");
        writer.println("<head><title>禁止访问</title></head>");
        writer.println("<body>");
        writer.println("<h1>403: Forbidden</h1>");
        writer.println("<h2>请使用前端页面，而不是试图别的方式</h2>");
        writer.println("<p>用户信息有误</p>");
        writer.println("<h2>访问失败</h2>");
        writer.println("</body>");
        writer.println("</html>");
    };
}
