package luo.hello.java.web.service.user.register;

import com.mysql.cj.xdevapi.JsonArray;
import com.mysql.cj.xdevapi.JsonNumber;
import luo.hello.java.web.db.model.UserModel;
import luo.hello.java.web.service.user.AbstractUserServlet;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.util.Collection;

@WebServlet("/all_phone")
public class AllPhoneNumberServlet extends AbstractUserServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        Collection<UserModel> userList;
        try {
            userList = usersDao.findAll();
        } catch (
                SQLException e) {
            throw new RuntimeException(e);
        }
        JsonArray jsonArray = new JsonArray();
        userList.forEach((UserModel user) -> {
            JsonNumber jsonNumber = new JsonNumber();
            jsonNumber.setValue(user.tel());
            jsonArray.add(jsonNumber);
        });
        resp.setContentType("application/json");
        resp.setCharacterEncoding(StandardCharsets.UTF_8.toString());
        PrintWriter writer = resp.getWriter();
        writer.println(jsonArray);
    }
}
