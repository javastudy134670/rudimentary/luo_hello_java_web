package luo.hello.java.web.service.admin.clean;

import luo.hello.java.web.WriteResponse;
import luo.hello.java.web.db.Conn;
import org.apache.tomcat.util.http.fileupload.FileUtils;

import javax.servlet.ServletContext;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;

@WebServlet("/admin/clear")
public class clearServlet extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setContentType("text/html");
        resp.setCharacterEncoding(StandardCharsets.UTF_8.toString());
        PrintWriter writer = resp.getWriter();
        ServletContext context = getServletContext();
        String url = context.getInitParameter("mysqlURL");
        String username = context.getInitParameter("username");
        String password = context.getInitParameter("password");
        String image_path = context.getRealPath("user_image");
        File imageDir = new File(image_path);
        FileUtils.deleteDirectory(imageDir);
        Conn conn = new Conn();
        try {
            conn.connDB(url, username, password);
            conn.UpdateSql("DROP TABLE IF EXISTS users;");
            conn.UpdateSql("DROP TABLE IF EXISTS dirs");
            conn.UpdateSql("DROP TABLE IF EXISTS files");
            conn.UpdateSql("""
                    CREATE TABLE IF NOT EXISTS users
                    (
                        Id       INT PRIMARY KEY AUTO_INCREMENT,
                        username VARCHAR(50),
                        pass     VARCHAR(50),
                        tel      VARCHAR(11),
                        age      INT,
                        role    TEXT
                    )
                        ENGINE = InnoDB;""");
            conn.UpdateSql("""
                    CREATE TABLE IF NOT EXISTS dirs
                    (
                        Id       INT PRIMARY KEY AUTO_INCREMENT,
                        name     VARCHAR(50),
                        userId   INT,
                        dirId    INT
                    )
                        ENGINE = InnoDB;""");
            conn.UpdateSql("""
                    CREATE TABLE IF NOT EXISTS files
                    (
                        Id       INT PRIMARY KEY AUTO_INCREMENT,
                        type     VARCHAR(50),
                        filename VARCHAR(50),
                        userId   INT,
                        dirId    INT
                    )
                        ENGINE = InnoDB;""");
            conn.closeDB();
            req.getSession().removeAttribute("USER_INFO");
            getServletContext().removeAttribute("aliveUsers");
        } catch (SQLException e) {
            e.printStackTrace();
            resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            Conn.sqlError.write(writer);
            return;
        }
        success.write(writer);
    }

    WriteResponse success = writer -> {
        writer.println("<html lang='zh'>");
        writer.println("<head> <title>清空数据</title> </head>");
        writer.println("<body>");
        writer.println("<h1> 成功 </h1>");
        writer.println("<p> 后台数据已清除 </p>");
        writer.println("</body>");
        writer.println("</html>");
    };
}
