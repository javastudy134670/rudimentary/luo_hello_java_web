package luo.hello.java.web.service.admin.listuser;

import luo.hello.java.web.db.model.UserModel;
import luo.hello.java.web.service.user.AbstractUserServlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Collection;

@WebServlet("/admin/userlist")
public class ListUsersServlet extends AbstractUserServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Collection<UserModel> userList;
        try {
            userList = usersDao.findAll();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        req.setAttribute("userList", userList);
        req.setAttribute("aliveUsers", getServletContext().getAttribute("aliveUsers"));
        getServletContext().getRequestDispatcher("/userlist.jsp").forward(req, resp);
    }
}
