package luo.hello.java.web.service.user;


import luo.hello.java.web.db.dao.DirDao;
import luo.hello.java.web.db.dao.UsersDao;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServlet;
import java.sql.SQLException;

public abstract class AbstractUserServlet extends HttpServlet {
    protected UsersDao usersDao = null;
    protected DirDao dirDao = null;

    @Override
    public void init() {
        ServletContext context = getServletContext();
        String url = context.getInitParameter("mysqlURL");
        String username = context.getInitParameter("username");
        String password = context.getInitParameter("password");
        try {
            usersDao = new UsersDao(url, username, password);
            dirDao = new DirDao(url, username, password);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}
