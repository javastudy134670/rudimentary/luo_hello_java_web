package luo.hello.java.web.service.user.logout;

import luo.hello.java.web.service.user.AbstractUserServlet;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/logout")
public class LogoutServlet extends AbstractUserServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        req.getSession().removeAttribute("USER_INFO");
        resp.sendRedirect("login.html");
    }
}
