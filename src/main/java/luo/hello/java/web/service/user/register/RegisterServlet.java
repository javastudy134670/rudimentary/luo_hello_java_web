package luo.hello.java.web.service.user.register;

import luo.hello.java.web.WriteResponse;
import luo.hello.java.web.db.model.DirModel;
import luo.hello.java.web.db.model.UserModel;
import luo.hello.java.web.service.user.AbstractUserServlet;
import org.apache.tomcat.util.http.fileupload.FileItemIterator;
import org.apache.tomcat.util.http.fileupload.FileItemStream;
import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

import static luo.hello.java.web.db.Conn.sqlError;

@WebServlet("/register")
public class RegisterServlet extends AbstractUserServlet {
    String username = null;
    String password = null;
    String phone = null;
    String age = null;
    String role = "normal";

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        ServletFileUpload upload = new ServletFileUpload();
        FileItemIterator iterator = upload.getItemIterator(request);
        while (iterator.hasNext()) {
            // 获取下一个文件或表单项
            FileItemStream item = iterator.next();
            // 如果是文件项，则打印文件名和大小
            if (!item.isFormField()) {
                String realPath = getServletContext().getRealPath("user_image");
                String filepath = "%s/%s.png".formatted(realPath, request.getAttribute("username"));
                File dir = new File(realPath);
                File file = new File(filepath);
                if (!dir.exists() && !dir.mkdirs()) {
                    throw new RuntimeException("创建目录失败");
                }
                if (!file.exists() && !file.createNewFile()) {
                    throw new RuntimeException("创建文件失败");
                }
                FileOutputStream outputStream = new FileOutputStream(file, false);
                InputStream inputStream = item.openStream();
                outputStream.write(inputStream.readAllBytes());
                inputStream.close();
                outputStream.close();
            } else {
                // 如果是表单项，则获取字段名称和值并将其添加到请求体中
                String fieldName = item.getFieldName();
                InputStream inputStream = item.openStream();
                String fieldValue = new String(inputStream.readAllBytes(), StandardCharsets.UTF_8);
                request.setAttribute(fieldName, fieldValue);
                inputStream.close();
            }
        }
        username = request.getAttribute("username").toString().trim();
        password = request.getAttribute("password").toString();
        phone = request.getAttribute("phone").toString();
        age = request.getAttribute("age").toString();
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter writer = response.getWriter();
        try {
            Collection<UserModel> users = usersDao.findAll();
            Collection<String> usernames = new ArrayList<>();
            Collection<String> phones = new ArrayList<>();
            users.forEach(user -> {
                usernames.add(user.username());
                phones.add(user.tel());
            });
            if (usernames.contains(username) || phones.contains(phone)) {
                response.setStatus(HttpServletResponse.SC_FORBIDDEN);
                forbidden.write(writer);
                return;
            }
            if (users.isEmpty()) role = "super";
        } catch (SQLException e) {
            response.setStatus(500);
            sqlError.write(writer);
            e.printStackTrace();
            return;
        }
        try {
            UserModel user = new UserModel(0, username, password, phone, Integer.parseInt(age), role);
            usersDao.insert(user);
            user=usersDao.findByName(username);
            dirDao.insert(new DirModel(0, "files", user.id(), 0));
            request.getSession().setAttribute("USER_INFO", user);
        } catch (Exception e) {
            e.printStackTrace();
            sqlError.write(writer);
            return;
        }
        success.write(writer);
    }

    WriteResponse success = writer -> {
        writer.println("<html lang='zh'>");
        writer.println("<head><title>注册结果</title></head>");
        writer.println("<body>");
        writer.println("<h2>注册结果</h2>");
        writer.println("<p>用户名：" + username + "</p>");
        writer.println("<p>电&emsp;话：" + phone + "</p");
        writer.println("<p>年&emsp;龄：" + age + "</p>");
        writer.println("<p>角&emsp;色：" + role + "</p>");
        writer.println("<div><img src='user_image/%s.png' alt='头像' width=200></div>".formatted(username));
        writer.println("<h2>注册成功！</h2>");
        writer.println("</body>");
        writer.println("</html>");
    };

    WriteResponse forbidden = writer -> {
        writer.println("<html lang='zh'>");
        writer.println("<head><title>注册结果</title></head>");
        writer.println("<body>");
        writer.println("<h1>403: Forbidden</h1>");
        writer.println("<h2>请使用前端页面注册，而不是试图别的渠道提交注册表单</h2>");
        writer.println("<p>用户重复或者手机号重复</p>");
        writer.println("<h2>注册失败</h2>");
        writer.println("</body>");
        writer.println("</html>");
    };
}
