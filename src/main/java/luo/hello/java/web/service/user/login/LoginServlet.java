package luo.hello.java.web.service.user.login;

import com.alibaba.fastjson2.JSONObject;
import luo.hello.java.web.db.model.UserModel;
import luo.hello.java.web.service.user.AbstractUserServlet;
import org.apache.tomcat.util.http.fileupload.FileItemIterator;
import org.apache.tomcat.util.http.fileupload.FileItemStream;
import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;

@WebServlet("/login")
public class LoginServlet extends AbstractUserServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setContentType("application/json");
        PrintWriter writer = resp.getWriter();
        JSONObject json = new JSONObject();
        ServletFileUpload upload = new ServletFileUpload();
        FileItemIterator iterator = upload.getItemIterator(req);
        while (iterator.hasNext()) {
            // 获取下一个文件或表单项
            FileItemStream item = iterator.next();
            // 如果是文件项，则打印文件名和大小
            if (item.isFormField()) {
                // 如果是表单项，则获取字段名称和值并将其添加到请求体中
                String fieldName = item.getFieldName();
                InputStream inputStream = item.openStream();
                String fieldValue = new String(inputStream.readAllBytes(), StandardCharsets.UTF_8);
                req.setAttribute(fieldName, fieldValue);
                inputStream.close();
            }
        }
        String username = req.getAttribute("username").toString().trim();
        String password = req.getAttribute("password").toString();
        UserModel user;
        try {
            user = usersDao.findByName(username);
        } catch (SQLException e) {
            json.put("message", "sql error");
            writer.println(json);
            return;
        }
        if (user != null && user.pass().equals(password)) {
            req.getSession().setAttribute("USER_INFO", user);
            json.put("success", "success");
        } else {
            json.put("message", "登录失败，用户名或密码错误");
        }
        writer.println(json);
    }
}
