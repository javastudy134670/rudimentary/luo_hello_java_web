package luo.hello.java.web.service.user.cancel;

import luo.hello.java.web.db.model.UserModel;
import luo.hello.java.web.service.user.AbstractUserServlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet("/cancel")
public class CancelServlet extends AbstractUserServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        UserModel user = (UserModel) req.getSession().getAttribute("USER_INFO");
        if(user.role().equals("super")){
            resp.setStatus(400);    //super 禁止被删除
            return;
        }
        try {
            usersDao.delete(user.id());
        } catch (SQLException e) {
            resp.setStatus(500);
            return;
        }
        req.getRequestDispatcher("logout").forward(req, resp);
    }
}
