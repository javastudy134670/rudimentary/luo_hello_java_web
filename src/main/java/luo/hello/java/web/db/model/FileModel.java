package luo.hello.java.web.db.model;

public record FileModel(
        int id,
        String type,
        String filename,
        int userId,
        int dirId
) {
    @Override
    public String toString() {
        return filename;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof FileModel file) {
            return file.id == id;
        }
        return false;
    }
}
