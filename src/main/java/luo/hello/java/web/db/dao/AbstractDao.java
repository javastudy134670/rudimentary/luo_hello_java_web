package luo.hello.java.web.db.dao;

import luo.hello.java.web.db.Conn;

import java.sql.SQLException;
import java.util.Collection;

/**
 * 数据表操作抽象类
 *
 * @param <T>
 */
public abstract class AbstractDao<T> {
    protected final String url;
    protected final String username;
    protected final String password;
    protected final Conn conn = new Conn();

    public AbstractDao(String url, String username, String password) {
        this.url = url;
        this.username = username;
        this.password = password;
    }

    /**
     * 增
     */
    public abstract void insert(T t) throws SQLException;

    /**
     * 删
     */
    public abstract void delete(int id) throws SQLException;

    /**
     * 改
     */
    public abstract void update(T t) throws SQLException;

    /**
     * 查（查所有）
     */
    public abstract Collection<T> findAll() throws SQLException;

    /**
     * 查（基于ID查单条）
     */
    public abstract T findById(int id) throws SQLException;

}
