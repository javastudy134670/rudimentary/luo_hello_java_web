package luo.hello.java.web.db.model;

public record DirModel(
        int id,
        String name,
        int userId,
        int dirId
) {
    @Override
    public String toString() {
        return name;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof DirModel dir) {
            return dir.id == id;
        }
        return false;
    }
}
