package luo.hello.java.web.db.dao;

import luo.hello.java.web.db.model.UserModel;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

public class UsersDao extends AbstractDao<UserModel> {
    public UsersDao(String url, String username, String password) throws SQLException {
        super(url, username, password);
        conn.connDB(url, username, password);
        conn.UpdateSql("""
                CREATE TABLE IF NOT EXISTS users
                (
                    Id       INT PRIMARY KEY AUTO_INCREMENT,
                    username VARCHAR(50),
                    pass     VARCHAR(50),
                    tel      VARCHAR(11),
                    age      INT,
                    role    TEXT
                )
                    ENGINE = InnoDB;""");
        conn.closeDB();
    }

    @Override
    public void insert(UserModel user) throws SQLException {
        String sql = "INSERT INTO users(username, pass, tel, age, role)VALUES ('%s', '%s', '%s', %d, '%s');".formatted(
                user.username(), user.pass(), user.tel(), user.age(), user.role());
        conn.connDB(url, username, password);
        conn.UpdateSql(sql);
        conn.closeDB();
    }

    @Override
    public void delete(int id) throws SQLException {
        String sql = "DELETE FROM users WHERE Id = %d;".formatted(id);
        conn.connDB(url, username, password);
        conn.UpdateSql(sql);
        conn.closeDB();
    }

    @Override
    public void update(UserModel user) throws SQLException {
        String sql = "UPDATE users SET username='%s', pass='%s', tel='%s', age=%d, role='%s' WHERE Id = %d;".formatted(
                user.username(), user.pass(), user.tel(), user.age(), user.role(), user.id()
        );
        conn.connDB(url, username, password);
        conn.UpdateSql(sql);
        conn.closeDB();
    }

    @Override
    public Collection<UserModel> findAll() throws SQLException {
        Collection<UserModel> results = new ArrayList<>();
        String sql = "SELECT Id, username, pass, tel, age, role FROM users;";
        conn.connDB(url, username, password);
        ResultSet rs = conn.SelectSql(sql);
        while (rs.next()) {
            results.add(new UserModel(
                    rs.getInt(1),
                    rs.getString(2),
                    rs.getString(3),
                    rs.getString(4),
                    rs.getInt(5),
                    rs.getString(6)
            ));
        }
        conn.closeDB();
        return results;
    }

    @Override
    public UserModel findById(int id) throws SQLException {
        String sql = "SELECT Id, username, pass, tel, age, role FROM users WHERE Id = %d;".formatted(id);
        return selectUser(sql);
    }

    public UserModel findByName(String username_param) throws SQLException {
        String sql = "SELECT Id, username, pass, tel, age, role FROM users WHERE username = '%s';".formatted(username_param);
        return selectUser(sql);
    }

    private UserModel selectUser(String sql) throws SQLException {
        conn.connDB(url, username, password);
        ResultSet rs = conn.SelectSql(sql);
        UserModel user = null;
        if (rs.next()) {
            user = new UserModel(
                    rs.getInt(1),
                    rs.getString(2),
                    rs.getString(3),
                    rs.getString(4),
                    rs.getInt(5),
                    rs.getString(6)
            );
        }
        conn.closeDB();
        return user;
    }
}
