package luo.hello.java.web.db.dao;

import luo.hello.java.web.db.model.FileModel;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

public class FileDao extends AbstractDao<FileModel> {
    public FileDao(String url, String username, String password) throws SQLException {
        super(url, username, password);
        conn.connDB(url, username, password);
        conn.UpdateSql("""
                CREATE TABLE IF NOT EXISTS files
                (
                    Id       INT PRIMARY KEY AUTO_INCREMENT,
                    type     VARCHAR(50),
                    filename VARCHAR(50),
                    userId   INT,
                    dirId    INT
                )
                    ENGINE = InnoDB;""");
        conn.closeDB();
    }

    @Override
    public void insert(FileModel file) throws SQLException {
        String sql = "INSERT INTO files(type, filename, userId, dirId)VALUES ('%s', '%s', %d, %d);".formatted(
                file.type(), file.filename(), file.userId(), file.dirId());
        conn.connDB(url, username, password);
        conn.UpdateSql(sql);
        conn.closeDB();
    }

    @Override
    public void delete(int id) throws SQLException {
        String sql = "DELETE FROM files WHERE Id = %d;".formatted(id);
        conn.connDB(url, username, password);
        conn.UpdateSql(sql);
        conn.closeDB();
    }

    @Override
    public void update(FileModel file) throws SQLException {
        String sql = "UPDATE files SET type='%s', filename='%s', userId=%d, dirId=%d WHERE Id = %d;".formatted(
                file.type(), file.filename(), file.userId(), file.dirId(), file.id()
        );
        conn.connDB(url, username, password);
        conn.UpdateSql(sql);
        conn.closeDB();
    }

    @Override
    public Collection<FileModel> findAll() throws SQLException {
        Collection<FileModel> results = new ArrayList<>();
        String sql = "SELECT Id, type, filename, userId, dirId FROM files;";
        conn.connDB(url, username, password);
        ResultSet rs = conn.SelectSql(sql);
        while (rs.next()) {
            results.add(new FileModel(
                    rs.getInt(1),
                    rs.getString(2),
                    rs.getString(3),
                    rs.getInt(4),
                    rs.getInt(5)
            ));
        }
        conn.closeDB();
        return results;
    }

    @Override
    public FileModel findById(int id) throws SQLException {
        String sql = "SELECT Id, type, filename, userId, dirId FROM users WHERE Id = %d;".formatted(id);
        FileModel file = null;
        conn.connDB(url, username, password);
        ResultSet rs = conn.SelectSql(sql);
        if (rs.next()) {
            file = new FileModel(
                    rs.getInt(1),
                    rs.getString(2),
                    rs.getString(3),
                    rs.getInt(4),
                    rs.getInt(5)
            );
        }
        conn.closeDB();
        return file;
    }
}
