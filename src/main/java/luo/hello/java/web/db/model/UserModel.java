package luo.hello.java.web.db.model;

public record UserModel(
        int id,
        String username,
        String pass,
        String tel,
        int age,
        String role
) {
    @Override
    public String toString() {
        return username;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof UserModel user) {
            return user.id == id;
        }
        return false;
    }
}
