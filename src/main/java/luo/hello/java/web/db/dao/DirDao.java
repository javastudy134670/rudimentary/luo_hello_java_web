package luo.hello.java.web.db.dao;

import luo.hello.java.web.db.model.DirModel;
import org.jetbrains.annotations.Nullable;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

public class DirDao extends AbstractDao<DirModel> {
    public DirDao(String url, String username, String password) throws SQLException {
        super(url, username, password);
        conn.connDB(url, username, password);
        conn.UpdateSql("""
                CREATE TABLE IF NOT EXISTS dirs
                (
                    Id       INT PRIMARY KEY AUTO_INCREMENT,
                    name     VARCHAR(50),
                    userId   INT,
                    dirId    INT
                )
                    ENGINE = InnoDB;""");
        conn.closeDB();
    }

    @Override
    public void insert(DirModel dir) throws SQLException {
        String sql = "INSERT INTO dirs(name, userId, dirId)VALUES ('%s', '%s', %d);".formatted(
                dir.name(), dir.userId(), dir.dirId());
        conn.connDB(url, username, password);
        conn.UpdateSql(sql);
        conn.closeDB();
    }

    @Override
    public void delete(int id) throws SQLException {
        String sql = "DELETE FROM dirs WHERE Id = %d;".formatted(id);
        conn.connDB(url, username, password);
        conn.UpdateSql(sql);
        conn.closeDB();
    }

    @Override
    public void update(DirModel dir) throws SQLException {
        String sql = "UPDATE dirs SET name='%s', userId=%d, dirId=%d WHERE Id = %d;".formatted(
                dir.name(), dir.userId(), dir.dirId(), dir.id()
        );
        conn.connDB(url, username, password);
        conn.UpdateSql(sql);
        conn.closeDB();
    }

    @Override
    public Collection<DirModel> findAll() throws SQLException {
        Collection<DirModel> results = new ArrayList<>();
        String sql = "SELECT Id, name, userId, dirId FROM dirs;";
        conn.connDB(url, username, password);
        ResultSet rs = conn.SelectSql(sql);
        while (rs.next()) {
            results.add(new DirModel(
                    rs.getInt(1),
                    rs.getString(2),
                    rs.getInt(3),
                    rs.getInt(4)
            ));
        }
        conn.closeDB();
        return results;
    }

    @Override
    public DirModel findById(int id) throws SQLException {
        String sql = "SELECT Id, name, userId, dirId FROM dirs WHERE Id = %d;".formatted(id);
        return findDirModel(sql);
    }

    public DirModel findByName(String name, int userId) throws SQLException {
        String sql = "SELECT Id, name, userId, dirId FROM dirs WHERE name = '%s' and userId = %d;".formatted(name, userId);
        return findDirModel(sql);
    }

    @Nullable
    private DirModel findDirModel(String sql) throws SQLException {
        DirModel dir = null;
        conn.connDB(url, username, password);
        ResultSet rs = conn.SelectSql(sql);
        if (rs.next()) {
            dir = new DirModel(
                    rs.getInt(1),
                    rs.getString(2),
                    rs.getInt(3),
                    rs.getInt(4)
            );
        }
        conn.closeDB();
        return dir;
    }

}
