package luo.hello.java.web.db;

import luo.hello.java.web.WriteResponse;
import org.jetbrains.annotations.NotNull;

import java.sql.*;

public class Conn {
    private Connection connection = null;
    private Statement statement = null;
    private ResultSet resultSet = null;

    public static WriteResponse sqlError = writer -> {
        writer.println("<html lang='zh'>");
        writer.println("<head><title>500 错误</title></head>");
        writer.println("<body>");
        writer.println("<h1>500 数据库错误</h1>");
        writer.println("<p>发生了一些内部错误</p>");
        writer.println("</body>");
        writer.println("</html>");
    };

    public void connDB(String url, String username, String password) throws SQLException {
        connection = DriverManager.getConnection(url, username, password);
    }

    public void closeDB() throws SQLException {
        if (resultSet != null) {
            resultSet.close();
            resultSet = null;
        }
        if (statement != null) {
            statement.close();
            statement = null;
        }
        if (connection != null) {
            connection.close();
            connection = null;
        }
    }

    public ResultSet SelectSql(String sql) throws SQLException {
        if (sql == null || sql.isBlank()) {
            return null;
        }
        sql = sql.trim();
        statement = connection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
        resultSet = statement.executeQuery(sql);
        return resultSet;
    }

    public void UpdateSql(@NotNull String sql) throws SQLException {
        statement = connection.createStatement();
        statement.executeUpdate(sql.trim());
    }

}
