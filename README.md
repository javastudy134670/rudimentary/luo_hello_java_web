# Java Web

2023/6/4 更新
用bootstrap库优化了register，完成了login。加入了context监听器定义销毁行为，销毁jdbc Driver对象。
加入了session和过滤器，来跟踪会话，查看前端是否处于登录状态。用bootstrap 库优化了index界面。

![](screenshots/优化后的注册界面.png)

![](screenshots/login.png)

2023/5/30 更新
超级更新了register，通过JS前端技术，注册可以用cropper裁剪图片上传图片信息。
前后端分别后端支持了查重用户名查重和手机号查重。可以列出所有用户。加入了首页导航索引。

其中查重和发送表单用到了JS的Fetch API，可以在前端异步请求服务器数据。
处理图片截取头像用到了JS的Cropper 工具。
POST到后端的请求藏在请求体里，用FileUploader来解析请求体表单数据。

![](screenshots/index.png)

![](screenshots/注册成功.png)

![](screenshots/用户列表.png)

![](screenshots/注册前端界面.png)

![](screenshots/前端查重.png)

![](screenshots/后端查重.png)

2023/5/26 更新
完成register。连通了mysql

![](screenshots/register.png)

![](screenshots/register_success.png)

![](screenshots/mysql.png)
