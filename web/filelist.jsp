<%--
  Created by IntelliJ IDEA.
  User: V4
  Date: 2023/6/16
  Time: 18:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" %>
<jsp:useBean id="user" scope="request" type="luo.hello.java.web.db.model.UserModel"/>
<jsp:useBean id="files" scope="request" type="java.util.List<luo.hello.java.web.db.model.FileModel>"/>
<jsp:useBean id="dirs" scope="request" type="java.util.List<luo.hello.java.web.db.model.DirModel>"/>
<jsp:useBean id="dirModel" scope="request" type="luo.hello.java.web.db.model.DirModel"/>

<html lang="zh">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>${user.username()}的文件家园</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css">
    <style>
        .card {
            transition: transform 0.3s;
        }

        .card:hover {
            transform: scale(1.1);
            z-index: 1;
        }
    </style>
</head>
<body>
<div class="container my-5">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb p-3 bg-body-tertiary rounded-3">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item"><a href="#">Library</a></li>
            <li class="breadcrumb-item active" aria-current="page">Data</li>
        </ol>
    </nav>
</div>
<div class="container my-5">
    <div class="container">
        <h5>子目录列表</h5>
    </div>
    <button class="btn btn-primary rounded-pill px-3" type="button">Primary</button>
    <button type="button" class="btn btn-info dropdown-toggle rounded-pill px-3" data-bs-toggle="dropdown"
            aria-expanded="false">
        Creat
    </button>
    <ul class="dropdown-menu">
        <li><a class="dropdown-item" href="#">创建文件</a></li>
        <li><a class="dropdown-item" href="#" data-bs-toggle="modal" data-bs-target="#creat-dir-modal">创建目录</a></li>
    </ul>

</div>
<div class="album py-5 bg-body-tertiary">
    <div class="container">

        <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3">
            <div class="col">
                <div class="card shadow-sm">
                    <img src="boop_by_jumblehorse_de0dmva-fullview.jpg" class="card-img-top" alt="">
                    <div class="card-body">
                        <p class="card-text">This is a wider card with supporting text below as a natural lead-in to
                            additional content. This content is a little longer.</p>
                        <div class="d-flex justify-content-between align-items-center">
                            <div class="btn-group">
                                <button type="button" class="btn btn-sm btn-outline-secondary">Download</button>
                                <button type="button" class="btn btn-sm btn-outline-secondary">View</button>
                            </div>
                            <small class="text-body-secondary">9 mins</small>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<div class="modal fade" id="creat-dir-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content rounded-4 shadow">
            <div class="modal-header p-5 pb-4 border-bottom-0">
                <h1 class="fw-bold mb-0 fs-2">新建目录</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form class="">
                    <div class="form-floating mb-3">
                        <input type="text" class="form-control rounded-3" id="dir_name_input" placeholder="请输入目录名称">
                        <label for="dir_name_input">输入目录名称</label>
                    </div>
                    <button class="w-100 mb-2 btn btn-lg rounded-3 btn-primary" type="submit">创建</button>
                    <button class="w-100 mb-2 btn btn-lg rounded-3 btn-secondary" data-bs-dismiss="modal">取消</button>
                    <small class="text-body-secondary">By clicking Sign up, you agree to the terms of use.</small>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="https://cdn.bootcdn.net/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>

</body>
</html>
