<%@ page import="luo.hello.java.web.db.model.UserModel" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<%
    UserModel user = (UserModel) session.getAttribute("USER_INFO");
    String image_path = "user_image/%s.png".formatted(user.username());
%>
<html lang="zh-CN">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>页面导航</title>
    <!-- 引入bootstrap样式表 -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css">
</head>
<body>
<!-- 创建一个导航栏容器 -->
<nav class="navbar navbar-expand-md navbar-dark bg-dark">
    <div class="container-fluid">
        <a class="navbar-brand" href="#" data-bs-toggle="modal" data-bs-target="#user-modal">
            <img src="<%out.print(image_path);%>" width="30" height="30" alt="">
            用户信息
        </a>
        <!-- 创建一个导航栏切换按钮 -->
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                data-bs-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="切换导航">
            <span class="navbar-toggler-icon"></span>
        </button>
        <!-- 创建一个导航栏折叠区域 -->
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- 创建一个导航栏菜单 -->
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item active">
                    <a class="nav-link" href="#"
                       onclick="showPage('<%out.print("user/%s/files".formatted(user.username()));%>')">
                        我的文件
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#" onclick="showPage('uploadFile.jsp')">
                        上传文件
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                       data-bs-toggle="dropdown" aria-expanded="false">
                        更多
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <li><a class="dropdown-item" href="#">组件</a></li>
                        <li><a class="dropdown-item" href="#">实例</a></li>
                        <li>
                            <hr class="dropdown-divider">
                        </li>
                        <li><a class="dropdown-item" href="#">关于我们</a></li>
                    </ul>
                </li>
            </ul>
            <!-- 创建一个导航栏搜索框 -->
            <form class="d-flex">
                <input class="form-control me-2" type="search" placeholder="搜索" aria-label="搜索">
                <button class="btn btn-outline-success" type="submit">搜索</button>
            </form>
        </div>
    </div>
</nav>

<!-- 用户详情模态框 -->
<div class="modal fade" id="user-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">用户详情</h5>
                <button type="button" class="close" data-bs-dismiss="modal" aria-label="关闭">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <!-- 在这里添加用户详情的内容，例如 -->
                <div class="row">
                    <div class="col-4">
                        <img src="<%out.print(image_path);%>"
                             alt="用户头像" class="img-fluid rounded-circle">
                    </div>
                    <div class="col-8">
                        <p><strong>用户名：</strong><%out.print(user.username());%></p>
                        <p><strong>年&emsp;龄：</strong><%out.print(user.age());%></p>
                        <p><strong>电&emsp;话：</strong><%out.print(user.tel());%></p>
                        <p><strong>角&emsp;色：</strong><%out.print(user.role());%></p>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <!-- 添加一个退出登录的按钮 -->
                <button type="button" class="btn btn-danger" onclick="cancel()">注销用户</button>
                <button type="button" class="btn btn-warning" onclick="logout()">退出登录</button>
                <button type="button" class="btn btn-primary" data-bs-dismiss="modal">确认</button>
            </div>
        </div>
    </div>
</div>

<!-- 创建一个iframe标签，用来显示其他页面的内容 -->
<iframe id="content" src="" width="100%" height="600px"></iframe>

<!-- 引入jquery和bootstrap脚本 -->
<script src="https://cdn.bootcdn.net/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>

<!-- 定义一个JavaScript函数，用来根据参数改变iframe的src属性 -->
<script>
    function showPage(page) {
        document.getElementById("content").src = page;
    }

    function logout() {
        // 发送一个GET请求到/logout路径
        const xhr = new XMLHttpRequest();
        xhr.open("GET", "logout");
        xhr.send();
        // 当请求完成时，跳转到登录页面
        xhr.onload = () => {
            alert("退出成功");
            window.location.href = "login.html";
        };
    }

    function cancel() {
        const xhr = new XMLHttpRequest();
        xhr.open("GET", "cancel");
        xhr.send();
        xhr.onload = () => {
            alert("注销成功");
            window.location.href = "login.html";
        }
    }
</script>

</body>
</html>