<%--
  Created by IntelliJ IDEA.
  User: V4
  Date: 2023/5/27
  Time: 11:10
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:useBean id="userList" scope="request" type="java.util.List<luo.hello.java.web.db.model.UserModel>"/>
<jsp:useBean id="aliveUsers" scope="request" type="java.util.List<luo.hello.java.web.db.model.UserModel>"/>
<html lang="zh">
<head>
    <title>用户列表</title>
    <style>
        table {
            border-collapse: collapse;
            border: 1px solid black;
        }

        th, td {
            border: 1px solid black;
            padding: 10px;
        }
    </style>
</head>
<body>
<h1>用户列表</h1>
<table>
    <tr>
        <th>Id</th>
        <th>username</th>
        <th>password</th>
        <th>phone</th>
        <th>age</th>
        <th>role</th>
        <th>status</th>
    </tr>
    <%-- JSTL 遍历用户列表--%>
    <c:forEach items="${userList}" var="user">
        <tr>
            <td>${user.id()}</td>
            <td>${user.username()}</td>
            <td>${user.pass()}</td>
            <td>${user.tel()}</td>
            <td>${user.age()}</td>
            <td>${user.role()}</td>
            <c:if test="${aliveUsers.contains(user)}">
                <td><c:out value="在线"/></td>
            </c:if>
            <c:if test="${not aliveUsers.contains(user)}">
                <td><c:out value="离线"/></td>
            </c:if>
        </tr>
    </c:forEach>
</table>
</body>
</html>
