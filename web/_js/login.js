// 获取表单元素
const form = document.getElementById("form");
// 给表单添加提交事件的监听器
form.addEventListener("submit", function (event) {
    // 阻止表单的默认提交行为
    event.preventDefault();
    // 获取表单中的数据
    const formData = new FormData(form);
    // 使用 fetch API 发送 post 请求
    fetch("login", {
        method: "POST",
        body: formData,

    })
        // 将响应转换为 JSON 格式
        .then((response) => response.json())
        // 处理响应数据
        .then((data) => {
            // 如果响应数据中有 success 属性，表示登录成功
            if (data.success) {
                // 跳转到主页
                alert("登录成功！")
                window.location.href = "index.jsp";
            } else {
                // 否则，表示登录失败
                // 提示登录失败的信息
                alert(data.message);
            }
        })
        // 处理错误
        .catch((error) => {
            console.error(error);
        });
});