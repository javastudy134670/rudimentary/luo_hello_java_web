// 获取页面元素
const form = document.getElementById("form");
const username = document.getElementById("username");
const password = document.getElementById("password");
const phone = document.getElementById("phone");
const age = document.getElementById("age");
const imageInput = document.getElementById('image');
const preview = document.getElementById('preview');
const cropModal = document.getElementById('crop-modal');
const cropBtn = document.getElementById('crop-btn');
const resetBtn = document.getElementById('reset');

// 创建一个cropper实例
let cropper = null;

// 监听文件输入框的change事件
imageInput.addEventListener('change', function () {
    // 获取选择的文件
    const file = this.files[0];
    // 判断文件是否存在
    if (file) {
        // 读取文件内容
        const reader = new FileReader();
        reader.onload = function () {
            // 设置预览图片的src属性
            preview.src = reader.result;
            // 显示模态框
            $(cropModal).modal('show');
        };
        reader.readAsDataURL(file);
    }
});

// 监听模态框的shown事件
$(cropModal).on('shown.bs.modal', function () {
    // 初始化cropper
    cropper = new Cropper(preview, {
        aspectRatio: 1, // 设置裁剪比例为1:1
        viewMode: 1, // 设置视图模式为限制裁剪框在画布内
        autoCropArea: 0.8, // 设置自动裁剪区域为80%
    });
});

// 监听模态框的hidden事件
$(cropModal).on('hidden.bs.modal', function () {
    // 销毁cropper
    cropper.destroy();
    cropper = null;
});

// 监听裁剪按钮的click事件
cropBtn.addEventListener('click', function () {
    // 隐藏模态框
    $(cropModal).modal('hide');
    // 更新文件输入框的值和标签的文本
    imageInput.files[0] = dataURLtoBlob(cropper.getCroppedCanvas().toDataURL()); // 将Blob对象赋值给文件输入框的files属性的第一个元素
    imageInput.nextElementSibling.textContent = '已选择图片'; // 将文件名标签的文本改为已选择图片
    // 定义一个函数，将DataURL转换为Blob对象
    function dataURLtoBlob(dataURL) {
        const arr = dataURL.split(',');
        const mime = arr[0].match(/:(.*?);/)[1];
        const b_str = atob(arr[1]);
        let n = b_str.length;
        const u8arr = new Uint8Array(n);
        while (n--) {
            u8arr[n] = b_str.charCodeAt(n);
        }
        return new Blob([u8arr], {type: mime});
    }
});

// 监听重置按钮的click事件
resetBtn.addEventListener('click', () => {
    // 清空表单元素的值
    username.value = "";
    password.value = "";
    phone.value = "";
    age.value = "";
    preview.src = "";
    imageInput.value = "";
    imageInput.nextElementSibling.textContent = '未选择图片';
});


// 监听用户名输入框的失去焦点事件
username.addEventListener("blur", () => {
    // 使用Fetch API发送GET请求到后台，并获取所有已存在的用户名（实际应用中可以优化为只查询当前输入的用户名是否存在）
    fetch("all_username")
        .then(response => response.json()) // 将响应转换为JSON格式
        .then(data => {
            // 检查用户名是否已存在，如果是，提示用户更换并清空输入框（实际应用中根据需求进行调整）
            if (data.includes(username.value)) {
                alert("用户名已被占用，请更换");
                username.value = "";
            }
        })
        .catch(() => {
            username.value = "";
        });
});

//类似于用户名查重，进行电话号码查重。
phone.addEventListener("blur", () => {
    //使用Fetch 发送GET请求后台，获取所有已存在的电话号码
    fetch("all_phone")
        .then(response => response.json())
        .then(data => {
            if (data.includes(parseInt(phone.value))) {
                alert("该电话号码已经注册，请更换");
                phone.value = null;
            }
        }).catch(() => {
        username.value = null;
    });
});
// 监听表单的提交事件
form.addEventListener("submit", (event) => {
    // 阻止表单的默认提交行为
    event.preventDefault();
    // 获取表单元素的值，并将裁剪后的图片数据作为image属性的值
    let data = {
        "username": username.value,
        "password": password.value,
        "phone": phone.value,
        "age": age.value,
        "image": imageInput.files[0]
    };

    // 使用FormData对象将表单数据转换为键值对形式，并添加到请求体中（实际应用中应该根据服务器端接收数据的方式进行调整）
    let formData = new FormData();
    for (let key in data) {
        formData.append(key, data[key]);
    }

    // 使用Fetch API发送POST请求到后台，并处理响应结果
    fetch("register", {
        method: "POST",
        body: formData
    })
        .then(response => {
            if (response.ok) {
                alert("注册成功！");
                response.text().then(resp => document.write(resp));// 将HTML字符串写入当前页面
            } else {
                alert("请求失败，状态码：" + response.status);
                resetBtn.click();
            }
        })
        .catch(error => { // 如果发生错误，提示用户并保持当前页面（实际应用中根据需求进行调整）
            alert(error);
            resetBtn.click();
        });
});
